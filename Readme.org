#+TODO: TODO DOING LATER | DONE


* Checklist
** DONE Ensure begin-center and begin-right works
** LATER Add an option for inserting info table
** DONE Document in a tanglable org file
** DONE Links in header - point them to correct places
*** DONE Add one post to each
** DONE Myrmidon script for new blog post, blog publish
** DONE folder local sitemaps
** TODO pass
** Add yasnippets for btsp
** Blog info  yasnippet
** DONE Arabic font
** DONE Check if Roboto slab works without quotes in standalone files
* DONE Header links
** Academic
** Misc
** Readings
** Scripture
** Tech
* Posts
** Academic
*** Cheatsheet
** Misc
*** Pomodoros
*** Rules
*** Mental models
** Readings
*** DONE Malayalam Poems
CLOSED: [2019-05-16 Thu 19:07]
** Scripture
*** DONE 112001
CLOSED: [2019-05-16 Thu 18:59]
*** Al Fatiha
** Tech
*** F13
*** Mobile phone keyboard
*** Anki - kboard
*** Zettel - org
*** How I build this site
*** Emacs toggles
*** Supermemo 1-5
