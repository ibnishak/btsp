(require 'ox)
(require 's)
(require 'org-id)

(defgroup org-export-btsp nil
"Options for exporting Org mode files to HTML."
:tag "Org Export HTML"
:group 'org-export)

(org-export-define-derived-backend 'btsp 'html
  :translate-alist '((template . btsp-html-template)
		     (inner-template . org-btsp-inner-template)
		     (quote-block . org-btsp-quote-block)
		     (center-block . org-btsp-center-block)
		     (src-block . btsp-html-src-block))
  :menu-entry
  '(?x "Export to BTSP HTML"
       ((?h "As HTML file" org-btsp-export-to-html)))
  :options-alist
  '((:html-head "HTML_HEAD" nil org-btsp-head newline)
    (:with-toc nil nil 2)
    (:with-creator nil nil t)
    ;; Retrieve LaTeX header for fragments.
    (:latex-header "LATEX_HEADER" nil nil newline)))

(defcustom org-btsp-head "
	 <link href=\"/assets/css/stylesheet.css\" rel=\"stylesheet\" style=\"text/css\"/>
<link rel=\"icon\" href=\"/assets/favicon/favicon.png\" type=\"image/png\"/>
<link rel=\"shortcut icon\" href=\"/assets/favicon/favicon.png\" type=\"image/png\"/>
	 <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
	 <link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\" integrity=\"sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T\" crossorigin=\"anonymous\">

	 <script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\" integrity=\"sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo\" crossorigin=\"anonymous\"></script>
	 <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\" integrity=\"sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1\" crossorigin=\"anonymous\"></script>
	 <script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous\"></script>"

  "This part goes to the <head> portion of the document."


  :group 'org-export-btsp
  :version "24.4"
  :package-version '(Org . "8.0")
  :type 'string) 
;;;###autoload
(put 'org-btsp-head 'safe-local-variable 'stringp)

(defconst org-btsp-scripts
  "<script type=\"text/javascript\">
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById(\"gototop\").style.display = \"block\";
    } else {
        document.getElementById(\"gototop\").style.display = \"none\";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}


function fallbackCopyTextToClipboard(text) {
  var textArea = document.createElement(\"textarea\");
  textArea.value = text;
  document.body.appendChild(textArea);
  textArea.focus();
  textArea.select();

  try {
    var successful = document.execCommand('copy');
    var msg = successful ? 'successful' : 'unsuccessful';
    console.log('Fallback: Copying text command was ' + msg);
  } catch (err) {
    console.error('Fallback: Oops, unable to copy', err);
  }

  document.body.removeChild(textArea);
}
function copyTextToClipboard(text) {
  if (!navigator.clipboard) {
    fallbackCopyTextToClipboard(text);
    return;
  }
  navigator.clipboard.writeText(text).then(function() {
    console.log('Async: Copying to clipboard was successful!');
  }, function(err) {
    console.error('Async: Could not copy text: ', err);
  });
}
</script>
")

(defconst org-btsp-style-default
  "<style type=\"text/css\">
 <!--/*--><![CDATA[/*><!--*/
@import url('https://fonts.googleapis.com/css?family=Cookie');

.bd-navbar {
	min-height: 4rem;
	background-color: #563d7c;
	box-shadow: 0 .5rem 1rem rgba(0, 0, 0, .05), inset 0 -1px 0 rgba(0, 0, 0, .1)
}

@media (max-width:991.98px) {
	.bd-navbar {
		padding-right: .5rem;
		padding-left: .5rem
	}
	.bd-navbar .navbar-nav-scroll {
		max-width: 100%;
		height: 2.5rem;
		margin-top: .25rem;
		overflow: hidden
	}
	.bd-navbar .navbar-nav-scroll .navbar-nav {
		padding-bottom: 2rem;
		overflow-x: auto;
		white-space: nowrap;
		-webkit-overflow-scrolling: touch
	}
}

@media (min-width:768px) {
	@supports ((position: -webkit-sticky) or (position:sticky)) {
		.bd-navbar {
			position:-webkit-sticky;
			position: sticky;
			top: 0;
			z-index: 1071
		}
	}
}

.bd-navbar .navbar-nav .nav-link {
	padding-right: .5rem;
	padding-left: .5rem;
	color: #cbbde2
}

.bd-navbar .navbar-nav .nav-link.active,
.bd-navbar .navbar-nav .nav-link:hover {
	color: #fff;
	background-color: transparent
}

.bd-navbar .navbar-nav .nav-link.active {
	font-weight: 600
}

.bd-navbar .navbar-nav-svg {
	display: inline-block;
	width: 1rem;
	height: 1rem;
	vertical-align: text-top
}

.bd-navbar .dropdown-menu {
	font-size: .875rem
}

.bd-navbar .dropdown-item.active {
	font-weight: 600;
	color: #212529;
	background-color: transparent;
	background-image: url(\"data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 8 8'%3e%3cpath fill='%23292b2c' d='M2.3 6.73L.6 4.53c-.4-1.04.46-1.4 1.1-.8l1.1 1.4 3.4-3.8c.6-.63 1.6-.27 1.2.7l-4 4.6c-.43.5-.8.4-1.1.1z'/%3e%3c/svg%3e\");
	background-repeat: no-repeat;
	background-position: .4rem .6rem;
	background-size: .75rem .75rem
}

.bd-masthead {
	position: relative;
	padding: 3rem 15px
}

.bd-masthead h1 {
	font-size: 4rem;
	line-height: 1
}

@media (max-width:1200px) {
	.bd-masthead h1 {
		font-size: calc(1.525rem + 3.3vw)
	}
}

.bd-masthead .btn {
	padding: .8rem 2rem;
	font-weight: 600;
	font-size: 1.25rem
}

.bd-masthead .carbonad {
	margin-top: 0!important;
	margin-bottom: -3rem!important
}

@media (min-width:576px) {
	.bd-masthead {
		padding-top: 5rem;
		padding-bottom: 5rem
	}
	.bd-masthead .carbonad {
		margin-bottom: 0!important
	}
}

@media (min-width:768px) {
	.bd-masthead .carbonad {
		margin-top: 3rem!important
	}
}

.half-rule {
	width: 6rem;
	margin: 2.5rem 0
}

.masthead-followup .bd-clipboard {
	display: none
}

.masthead-followup .highlight {
	padding: .5rem 0;
	background-color: transparent
}

#carbonads {
	position: static;
	display: block;
	max-width: 400px;
	padding: 15px 15px 15px 160px;
	margin: 2rem 0;
	overflow: hidden;
	font-size: .8125rem;
	line-height: 1.4;
	text-align: left;
	background-color: rgba(0, 0, 0, .05)
}

#carbonads a {
	color: #333;
	text-decoration: none
}

@media (min-width:576px) {
	#carbonads {
		max-width: 330px;
		border-radius: 4px
	}
}

.carbon-img {
	float: left;
	margin-left: -145px
}

.carbon-poweredby {
	display: block;
	color: #777!important
}

.bd-content {
	-ms-flex-order: 1;
	order: 1
}

.bd-content>h2[id],
.bd-content>h3[id],
.bd-content>h4[id] {
	pointer-events: none
}

.bd-content>h2[id]::before,
.bd-content>h3[id]::before,
.bd-content>h4[id]::before {
	display: block;
	height: 6rem;
	margin-top: -6rem;
	content: \"\"
}

.bd-content>table {
	width: 100%;
	max-width: 100%;
	margin-bottom: 1rem
}

@media (max-width:991.98px) {
	.bd-content>table {
		display: block;
		overflow-x: auto
	}
	.bd-content>table.table-bordered {
		border: 0
	}
}

.bd-content>table>tbody>tr>td,
.bd-content>table>tbody>tr>th,
.bd-content>table>tfoot>tr>td,
.bd-content>table>tfoot>tr>th,
.bd-content>table>thead>tr>td,
.bd-content>table>thead>tr>th {
	padding: .75rem;
	vertical-align: top;
	border: 1px solid #dee2e6
}

.bd-content>table>tbody>tr>td>p:last-child,
.bd-content>table>tbody>tr>th>p:last-child,
.bd-content>table>tfoot>tr>td>p:last-child,
.bd-content>table>tfoot>tr>th>p:last-child,
.bd-content>table>thead>tr>td>p:last-child,
.bd-content>table>thead>tr>th>p:last-child {
	margin-bottom: 0
}

.bd-content>table td:first-child>code {
	white-space: nowrap
}

.bd-content-title {
	display: block;
	pointer-events: auto
}

.bd-content>h2 {
	font-size: 2rem
}

@media (max-width:1200px) {
	.bd-content>h2 {
		font-size: calc(1.325rem + .9vw)
	}
}

.bd-content>h3 {
	font-size: 1.75rem
}

@media (max-width:1200px) {
	.bd-content>h3 {
		font-size: calc(1.3rem + .6vw)
	}
}

.bd-content>h4 {
	font-size: 1.5rem
}

@media (max-width:1200px) {
	.bd-content>h4 {
		font-size: calc(1.275rem + .3vw)
	}
}

.bd-content>h2:not(:first-child) {
	margin-top: 3rem
}

.bd-content>h3 {
	margin-top: 1.5rem
}

.bd-content>ol li,
.bd-content>ul li {
	margin-bottom: .25rem
}

@media (min-width:992px) {
	.bd-content>ol,
	.bd-content>p,
	.bd-content>ul {
		max-width: 80%
	}
}

.bd-title {
	margin-top: 1rem;
	margin-bottom: .5rem;
	font-weight: 300;
	font-size: 3rem
}

@media (max-width:1200px) {
	.bd-title {
		font-size: calc(1.425rem + 2.1vw)
	}
}

.bd-lead {
	font-size: 1.5rem;
	font-weight: 300
}

@media (max-width:1200px) {
	.bd-lead {
		font-size: calc(1.275rem + .3vw)
	}
}

@media (min-width:992px) {
	.bd-lead {
		max-width: 80%
	}
}

.bd-text-purple {
	color: #563d7c
}

.bd-text-purple-bright {
	color: #7952b3
}

.skippy {
	display: block;
	padding: 1em;
	color: #fff;
	text-align: center;
	background-color: #563d7c;
	outline: 0
}

.skippy:hover {
	color: #fff
}

.skippy-text {
	padding: .5em;
	outline: 1px dotted
}

.bd-toc {
	-ms-flex-order: 2;
	order: 2;
	padding-top: 1.5rem;
	padding-bottom: 1.5rem;
	font-size: .875rem
}

@supports ((position:-webkit-sticky) or (position:sticky)) {
	.bd-toc {
		position: -webkit-sticky;
		position: sticky;
		top: 4rem;
		height: calc(100vh - 4rem);
		overflow-y: auto
	}
}

.section-nav {
	padding-left: 0;
	border-left: 1px solid #eee
}

.section-nav ul {
	padding-left: 1rem
}

.toc-entry {
	display: block
}

.toc-entry a {
	display: block;
	padding: .125rem 1.5rem;
	color: #77757a
}

.toc-entry a:hover {
	color: #007bff;
	text-decoration: none
}

.bd-sidebar {
	-ms-flex-order: 0;
	order: 0;
}

@media (min-width:768px) {
	.bd-sidebar {
		border-right: 1px solid rgba(0, 0, 0, .1);
                position: -webkit-sticky;
		position: sticky !important;
	}
	@supports ((position:-webkit-sticky) or (position:sticky)) {
		.bd-sidebar {
			position: -webkit-sticky;
			position: sticky;
			top: 4rem;
			z-index: 1000;
			height: calc(100vh - 4rem)
		}
	}
}

@media (min-width:1200px) {
	.bd-sidebar {
		-ms-flex: 0 1 320px;
		flex: 0 1 320px;f
	}
}

.bd-links {
	padding-top: 1rem;
	padding-bottom: 1rem;
	margin-right: -15px;
	margin-left: -15px
}

@media (min-width:768px) {
	@supports ((position: -webkit-sticky) or (position:sticky)) {
		.bd-links {
			max-height:calc(100vh - 9rem);
			overflow-y: auto
		}
	}
}

@media (min-width:768px) {
	.bd-links {
		display: block!important
	}
}

.bd-search {
	position: relative;
	padding: 1rem 15px;
	margin-right: -15px;
	margin-left: -15px;
	border-bottom: 1px solid rgba(0, 0, 0, .05)
}

.bd-search .form-control:focus {
	border-color: #7952b3;
	box-shadow: 0 0 0 3px rgba(121, 82, 179, .25)
}

.bd-search-docs-toggle {
	line-height: 1;
	color: #212529
}

.bd-sidenav {
	display: none
}

.bd-toc-link {
	display: block;
	padding: .25rem 1.5rem;
	font-weight: 600;
	color: rgba(0, 0, 0, .65)
}

.bd-toc-link:hover {
	color: rgba(0, 0, 0, .85);
	text-decoration: none
}

.bd-toc-item.active {
	margin-bottom: 1rem
}

.bd-toc-item.active:not(:first-child) {
	margin-top: 1rem
}

.bd-toc-item.active>.bd-toc-link {
	color: rgba(0, 0, 0, .85)
}

.bd-toc-item.active>.bd-toc-link:hover {
	background-color: transparent
}

.bd-toc-item.active>.bd-sidenav {
	display: block
}

.bd-sidebar .nav>li>a {
	display: block;
	padding: .25rem 1.5rem;
	font-size: 90%;
	color: rgba(0, 0, 0, .65)
}

.bd-sidebar .nav>li>a:hover {
	color: rgba(0, 0, 0, .85);
	text-decoration: none;
	background-color: transparent
}

.bd-sidebar .nav>.active:hover>a,
.bd-sidebar .nav>.active>a {
	font-weight: 600;
	color: rgba(0, 0, 0, .85);
	background-color: transparent
}

.bd-footer {
	font-size: .875rem;
	text-align: center;
	background-color: #f7f7f7
}

.bd-footer a {
	font-weight: 600;
	color: #495057
}

.bd-footer a:focus,
.bd-footer a:hover {
	color: #007bff
}

.bd-footer p {
	margin-bottom: 0
}

@media (min-width:576px) {
	.bd-footer {
		text-align: left
	}
}

.bd-footer-links {
	padding-left: 0;
	margin-bottom: 1rem
}

.bd-footer-links li {
	display: inline-block
}

.bd-footer-links li+li {
	margin-left: 1rem
}

.bd-example-row .row>.col,
.bd-example-row .row>[class^=col-] {
	padding-top: .75rem;
	padding-bottom: .75rem;
	background-color: rgba(86, 61, 124, .15);
	border: 1px solid rgba(86, 61, 124, .2)
}

.bd-example-row .row+.row {
	margin-top: 1rem
}

.bd-example-row .flex-items-bottom,
.bd-example-row .flex-items-middle,
.bd-example-row .flex-items-top {
	min-height: 6rem;
	background-color: rgba(255, 0, 0, .1)
}

.bd-example-row-flex-cols .row {
	min-height: 10rem;
	background-color: rgba(255, 0, 0, .1)
}

.bd-highlight {
	background-color: rgba(86, 61, 124, .15);
	border: 1px solid rgba(86, 61, 124, .15)
}

.example-container {
	width: 800px;
	width: 100%;
	padding-right: 15px;
	padding-left: 15px;
	margin-right: auto;
	margin-left: auto
}

.example-row {
	display: -ms-flexbox;
	display: flex;
	-ms-flex-wrap: wrap;
	flex-wrap: wrap;
	margin-right: -15px;
	margin-left: -15px
}

.example-content-main {
	position: relative;
	width: 100%;
	padding-right: 15px;
	padding-left: 15px
}

@media (min-width:576px) {
	.example-content-main {
		-ms-flex: 0 0 50%;
		flex: 0 0 50%;
		max-width: 50%
	}
}

@media (min-width:992px) {
	.example-content-main {
		-ms-flex: 0 0 66.666667%;
		flex: 0 0 66.666667%;
		max-width: 66.666667%
	}
}

.example-content-secondary {
	position: relative;
	width: 100%;
	padding-right: 15px;
	padding-left: 15px
}

@media (min-width:576px) {
	.example-content-secondary {
		-ms-flex: 0 0 50%;
		flex: 0 0 50%;
		max-width: 50%
	}
}

@media (min-width:992px) {
	.example-content-secondary {
		-ms-flex: 0 0 33.333333%;
		flex: 0 0 33.333333%;
		max-width: 33.333333%
	}
}

.bd-example-container {
	min-width: 16rem;
	max-width: 25rem;
	margin-right: auto;
	margin-left: auto
}

.bd-example-container-header {
	height: 3rem;
	margin-bottom: .5rem;
	background-color: #fff;
	border-radius: .25rem
}

.bd-example-container-sidebar {
	float: right;
	width: 4rem;
	height: 8rem;
	background-color: #80bdff;
	border-radius: .25rem
}

.bd-example-container-body {
	height: 8rem;
	margin-right: 4.5rem;
	background-color: #957bbe;
	border-radius: .25rem
}

.bd-example-container-fluid {
	max-width: none
}

.bd-example {
	position: relative;
	padding: 1rem;
	margin: 1rem -15px 0;
	border: solid #f8f9fa;
	border-width: .2rem 0 0
}

.bd-example::after {
	display: block;
	clear: both;
	content: \"\"
}

@media (min-width:576px) {
	.bd-example {
		padding: 1.5rem;
		margin-right: 0;
		margin-left: 0;
		border-width: .2rem
	}
}

.bd-example+.clipboard+.highlight,
.bd-example+.highlight {
	margin-top: 0
}

.bd-example+p {
	margin-top: 2rem
}

.bd-example .pos-f-t {
	position: relative;
	margin: -1rem
}

@media (min-width:576px) {
	.bd-example .pos-f-t {
		margin: -1.5rem
	}
}

.bd-example .custom-file-input:lang(es)~.custom-file-label::after {
	content: \"Elegir\"
}

.bd-example>.form-control+.form-control {
	margin-top: .5rem
}

.bd-example>.alert+.alert,
.bd-example>.nav+.nav,
.bd-example>.navbar+.navbar,
.bd-example>.progress+.btn,
.bd-example>.progress+.progress {
	margin-top: 1rem
}

.bd-example>.dropdown-menu:first-child {
	position: static;
	display: block
}

.bd-example>.form-group:last-child {
	margin-bottom: 0
}

.bd-example>.close {
	float: none
}

.bd-example-type .table td {
	padding: 1rem 0;
	border-color: #eee
}

.bd-example-type .table tr:first-child td {
	border-top: 0
}

.bd-example-type h1,
.bd-example-type h2,
.bd-example-type h3,
.bd-example-type h4,
.bd-example-type h5,
.bd-example-type h6 {
	margin-top: 0;
	margin-bottom: 0
}

.bd-example-bg-classes p {
	padding: 1rem
}

.bd-example>img+img,
.bd-example>svg+svg {
	margin-left: .5rem
}

.bd-example>.btn,
.bd-example>.btn-group {
	margin-top: .25rem;
	margin-bottom: .25rem
}

.bd-example>.btn-toolbar+.btn-toolbar {
	margin-top: .5rem
}

.bd-example-control-sizing input[type=text]+input[type=text],
.bd-example-control-sizing select {
	margin-top: .5rem
}

.bd-example-form .input-group {
	margin-bottom: .5rem
}

.bd-example>textarea.form-control {
	resize: vertical
}

.bd-example>.list-group {
	max-width: 400px
}

.bd-example>[class*=list-group-horizontal] {
	max-width: 100%
}

.bd-example .fixed-top,
.bd-example .sticky-top {
	position: static;
	margin: -1rem -1rem 1rem
}

.bd-example .fixed-bottom {
	position: static;
	margin: 1rem -1rem -1rem
}

@media (min-width:576px) {
	.bd-example .fixed-top,
	.bd-example .sticky-top {
		margin: -1.5rem -1.5rem 1rem
	}
	.bd-example .fixed-bottom {
		margin: 1rem -1.5rem -1.5rem
	}
}

.bd-example .pagination {
	margin-top: .5rem;
	margin-bottom: .5rem
}

.modal {
	z-index: 1072
}

.modal .popover,
.modal .tooltip {
	z-index: 1073
}

.modal-backdrop {
	z-index: 1071
}

.bd-example-modal {
	background-color: #fafafa
}

.bd-example-modal .modal {
	position: relative;
	top: auto;
	right: auto;
	bottom: auto;
	left: auto;
	z-index: 1;
	display: block
}

.bd-example-modal .modal-dialog {
	left: auto;
	margin-right: auto;
	margin-left: auto
}

.bd-example-tabs .nav-tabs {
	margin-bottom: 1rem
}

.bd-example-popover-static {
	padding-bottom: 1.5rem;
	background-color: #f9f9f9
}

.bd-example-popover-static .popover {
	position: relative;
	display: block;
	float: left;
	width: 260px;
	margin: 1.25rem
}

.tooltip-demo a {
	white-space: nowrap
}

.bd-example-tooltip-static .tooltip {
	position: relative;
	display: inline-block;
	margin: 10px 20px;
	opacity: 1
}

.scrollspy-example {
	position: relative;
	height: 200px;
	margin-top: .5rem;
	overflow: auto
}

.scrollspy-example-2 {
	position: relative;
	height: 350px;
	overflow: auto
}

.bd-example-border-utils [class^=border] {
	display: inline-block;
	width: 5rem;
	height: 5rem;
	margin: .25rem;
	background-color: #f5f5f5
}

.bd-example-border-utils-0 [class^=border] {
	border: 1px solid #dee2e6
}

.highlight {
	padding: 1rem;
	margin-top: 1rem;
	margin-bottom: 1rem;
	background-color: #f8f9fa;
	-ms-overflow-style: -ms-autohiding-scrollbar
}

@media (min-width:576px) {
	.highlight {
		padding: 1.5rem
	}
}

.bd-content .highlight {
	margin-right: -15px;
	margin-left: -15px
}

@media (min-width:576px) {
	.bd-content .highlight {
		margin-right: 0;
		margin-left: 0
	}
}

.highlight pre {
	padding: 0;
	margin-top: 0;
	margin-bottom: 0;
	background-color: transparent;
	border: 0
}

.highlight pre code {
	font-size: inherit;
	color: #212529
}

.btn-bd-primary {
	font-weight: 600;
	color: #7952b3;
	border-color: #7952b3
}

.btn-bd-primary:active,
.btn-bd-primary:hover {
	color: #fff;
	background-color: #7952b3;
	border-color: #7952b3
}

.btn-bd-primary:focus {
	box-shadow: 0 0 0 3px rgba(121, 82, 179, .25)
}

.btn-bd-download {
	font-weight: 600;
	color: #ffe484;
	border-color: #ffe484
}

.btn-bd-download:active,
.btn-bd-download:hover {
	color: #2a2730;
	background-color: #ffe484;
	border-color: #ffe484
}

.btn-bd-download:focus {
	box-shadow: 0 0 0 3px rgba(255, 228, 132, .25)
}

.bd-callout {
	padding: 1.25rem;
	margin-top: 1.25rem;
	margin-bottom: 1.25rem;
	border: 1px solid #eee;
	border-left-width: .25rem;
	border-radius: .25rem
}

.bd-callout h4 {
	margin-top: 0;
	margin-bottom: .25rem
}

.bd-callout p:last-child {
	margin-bottom: 0
}

.bd-callout code {
	border-radius: .25rem
}

.bd-callout+.bd-callout {
	margin-top: -.25rem
}

.bd-callout-info {
	border-left-color: #5bc0de
}

.bd-callout-info h4 {
	color: #5bc0de
}

.bd-callout-warning {
	border-left-color: #f0ad4e
}

.bd-callout-warning h4 {
	color: #f0ad4e
}

.bd-callout-danger {
	border-left-color: #d9534f
}

.bd-callout-danger h4 {
	color: #d9534f
}

.bd-browser-bugs td p {
	margin-bottom: 0
}

.bd-browser-bugs th:first-child {
	width: 18%
}

.bd-brand-logos {
	display: table;
	width: 100%;
	margin-bottom: 1rem;
	overflow: hidden;
	color: #563d7c;
	background-color: #f9f9f9;
	border-radius: .25rem
}

.bd-brand-logos .inverse {
	color: #fff;
	background-color: #563d7c
}

.bd-brand-item {
	padding: 4rem 0;
	text-align: center
}

.bd-brand-item+.bd-brand-item {
	border-top: 1px solid #fff
}

.bd-brand-item h1,
.bd-brand-item h3 {
	margin-top: 0;
	margin-bottom: 0
}

@media (min-width:768px) {
	.bd-brand-item {
		display: table-cell;
		width: 1%
	}
	.bd-brand-item+.bd-brand-item {
		border-top: 0;
		border-left: 1px solid #fff
	}
	.bd-brand-item h1 {
		font-size: 4rem
	}
}

@media (min-width:768px) and (max-width:1200px) {
	.bd-brand-item h1 {
		font-size: calc(1.525rem + 3.3vw)
	}
}

.color-swatches {
	margin: 0 -5px;
	overflow: hidden
}

.color-swatches .bd-purple {
	background-color: #563d7c
}

.color-swatches .bd-purple-light {
	background-color: #cbbde2
}

.color-swatches .bd-purple-lighter {
	background-color: #e5e1ea
}

.color-swatches .bd-gray {
	background-color: #f9f9f9
}

.color-swatch {
	float: left;
	width: 4rem;
	height: 4rem;
	margin-right: .25rem;
	margin-left: .25rem;
	border-radius: .25rem
}

@media (min-width:768px) {
	.color-swatch {
		width: 6rem;
		height: 6rem
	}
}

.swatch-blue {
	color: #fff;
	background-color: #007bff
}

.swatch-indigo {
	color: #fff;
	background-color: #6610f2
}

.swatch-purple {
	color: #fff;
	background-color: #6f42c1
}

.swatch-pink {
	color: #fff;
	background-color: #e83e8c
}

.swatch-red {
	color: #fff;
	background-color: #dc3545
}

.swatch-orange {
	color: #212529;
	background-color: #fd7e14
}

.swatch-yellow {
	color: #212529;
	background-color: #ffc107
}

.swatch-green {
	color: #fff;
	background-color: #28a745
}

.swatch-teal {
	color: #fff;
	background-color: #20c997
}

.swatch-cyan {
	color: #fff;
	background-color: #17a2b8
}

.swatch-white {
	color: #212529;
	background-color: #fff
}

.swatch-gray {
	color: #fff;
	background-color: #6c757d
}

.swatch-gray-dark {
	color: #fff;
	background-color: #343a40
}

.swatch-primary {
	color: #fff;
	background-color: #007bff
}

.swatch-secondary {
	color: #fff;
	background-color: #6c757d
}

.swatch-success {
	color: #fff;
	background-color: #28a745
}

.swatch-info {
	color: #fff;
	background-color: #17a2b8
}

.swatch-warning {
	color: #212529;
	background-color: #ffc107
}

.swatch-danger {
	color: #fff;
	background-color: #dc3545
}

.swatch-light {
	color: #212529;
	background-color: #f8f9fa
}

.swatch-dark {
	color: #fff;
	background-color: #343a40
}

.swatch-100 {
	color: #212529;
	background-color: #f8f9fa
}

.swatch-200 {
	color: #212529;
	background-color: #e9ecef
}

.swatch-300 {
	color: #212529;
	background-color: #dee2e6
}

.swatch-400 {
	color: #212529;
	background-color: #ced4da
}

.swatch-500 {
	color: #212529;
	background-color: #adb5bd
}

.swatch-600 {
	color: #fff;
	background-color: #6c757d
}

.swatch-700 {
	color: #fff;
	background-color: #495057
}

.swatch-800 {
	color: #fff;
	background-color: #343a40
}

.swatch-900 {
	color: #fff;
	background-color: #212529
}

.bd-clipboard {
	position: relative;
	float: right
}

.bd-clipboard+.highlight {
	margin-top: 0
}

@media (min-width:768px) {
	.bd-clipboard {
		display: block
	}
}

.btn-clipboard {
	position: absolute;
	top: .5rem;
	right: .5rem;
	z-index: 10;
	display: block;
	padding: .25rem .5rem;
	font-size: 75%;
	color: #818a91;
	background-color: transparent;
	border: 0;
	border-radius: .25rem
}

.btn-clipboard:hover {
	color: #fff;
	background-color: #027de7
}

.bd-placeholder-img {
	font-size: 1.125rem;
	text-anchor: middle;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none
}

.bd-placeholder-img-lg {
	font-size: 3.5rem
}

@media (max-width:1200px) {
	.bd-placeholder-img-lg {
		font-size: calc(1.475rem + 2.7vw)
	}
}

.anchorjs-link {
	font-weight: 400;
	color: rgba(0, 123, 255, .5);
	transition: color .15s ease-in-out, opacity .15s ease-in-out
}

@media (prefers-reduced-motion:reduce) {
	.anchorjs-link {
		transition: none
	}
}

.anchorjs-link:hover {
	color: #007bff;
	text-decoration: none
}


#text-table-of-contents > ul > li a {
	display: block;
	padding: .25rem 1.5rem;
        padding-inline-start: 20px;
	font-size: 90%;
	color: rgba(0,0,0,.85);
}
#text-table-of-contents > ul > li > ul > li > a {
	display: block;
	padding: .25rem 1.5rem;
	font-size: 90%;
	color: rgba(0,0,0,.65);
}
#text-table-of-contents > ul > li a:hover {
	color: #007bff;
        text-decoration: none;
        background-color: transparent;
}

#text-table-of-contents .badge {
        display:none;
}

h2 {
	margin-top: 3em !important;
}
h2 {
	border-bottom: 1px solid #f0c;
	padding-bottom: 0.5em;
	font-size: 1.75em;
}
h1, h2, h3 {
	font-family: \"Roboto Slab\", Helvetica, Arial, sans-serif;
}

#gototop {
    display: none; /* Hidden by default */
    position: fixed; /* Fixed/sticky position */
    bottom: 20px; /* Place the button at the bottom of the page */
    right: 30px; /* Place the button 30px from the right */
    width: 30px;
    height: 30px;
    z-index: 99; /* Make sure it does not overlap */
    border: none; /* Remove borders */
    outline: none; /* Remove outline */
    background-color: #F44336; /* Set a background color */
    color: white; /* Text color */
    cursor: pointer; /* Add a mouse pointer on hover */
    border-radius: 50%;
    font-size: 18px; /* Increase font size */
        -moz-transform: rotate(270deg);
    -webkit-transform: rotate(270deg);
    -o-transform: rotate(270deg);
    -ms-transform: rotate(270deg);
}

#gototop:hover {
    background-color: #555; /* Add a dark-grey background on hover */
}

pre {
    background-color: #272822;
    padding:1rem;
    color: #fff !important;
    margin-right: -15px;
    margin-left: -15px;
}

@media (min-width:576px) {
    pre {
	margin-right: 0;
	margin-left: 0;
        border-radius:2%;
    }
}
@media (max-width:576px) {
    pre {
	font-size:12px !important;
    }
}

div[class^=\"outline\"] h1::before, div[class^=\"outline\"] h2::before, div[class^=\"outline\"] h3::before {
display: block;
content: \" \";
margin-top: -5rem;
height: 5rem;
visibility: hidden;
pointer-events: none;
}

.alert p {
	margin: 0;
}
/*]]>*/-->
</style>"
  "The default style specification for exported HTML files.
You can use `org-html-head' and `org-html-head-extra' to add to
this style.  If you don't want to include this default style,
customize `org-html-head-include-default-style'.")

(defcustom nav-alist '((Home . /)
                (Tech . /posts/tech)
                (Readings . /posts/readings)
		(Scripture . /posts/scripture)
		(Acad . /posts/acad)
		(Misc . /posts/misc))
    "Each element is a list of the form (KEY VALUE)."
    :type '(alist :value-type (group integer))
    :group 'org-export-btsp
    :version "24.4"
    :package-version '(Org . "8.0")
    )

(defconst org-btsp-header
  (concat
  "<header class=\"navbar navbar-expand navbar-dark flex-column flex-md-row bd-navbar\">\n"
  "<a class=\"navbar-brand mr-0 mr-md-2\" href=\"/\" aria-label=\"ItsyBitsySpider\" style=\"font-family:'Cookie', cursive; font-size:28px\">ItsyBitsySpider</a>\n"
  "<div class=\"navbar-nav-scroll\">\n"
  "<ul class=\"navbar-nav bd-navbar-nav flex-row\">\n"
  (cl-loop for (key . value) in nav-alist
	   collect (format "<li class=\"nav-item\">\n\t  <a class=\"nav-link \" href=\"%s\">%s</a> \n</li>" value key) into nav-list
	   finally 
	   (return (mapconcat 'identity nav-list " ")))
  "</ul>\n"
  "</div>\n"
  "</header>\n")
  "Header for btsp")

(defun org-btsp-quote-block (quote-block contents _info)
  "Transcode a QUOTE-BLOCK element from Org to HTML.
CONTENTS holds the contents of the block.  INFO is a plist
holding contextual information."
  (format "<blockquote class=\"blockquote\" %s>\n<p class=\"mb-0\">%s</p>\n</blockquote>"
	  (let* ((name (org-element-property :name quote-block))
		 (attributes (org-export-read-attribute :attr_html quote-block))
		 (a (org-html--make-attribute-string
		     (if (or (not name) (plist-member attributes :id))
			 attributes
		       (plist-put attributes :id name)))))
	    (if (org-string-nw-p a) (concat " " a) ""))
	  contents))

(defun btsp-html-src-block (src-block contents info)
  "Transcode a SRC-BLOCK element from Org to HTML.
CONTENTS holds the contents of the item.  INFO is a plist holding
contextual information."
  (if (org-export-read-attribute :attr_html src-block :textarea)
      (org-html--textarea-block src-block)
    (let* ((lang (org-element-property :language src-block))
	  (caption (org-export-get-caption src-block))
	  (code (org-html-format-code src-block info))
	  (label (let ((lbl (and (org-element-property :name src-block)
				 (org-export-get-reference src-block info))))
		   (if lbl (format " id=\"%s\"" lbl)
		     (concat "btn_" (s-replace "-" "" (org-id-new))))))
 	  (btnprops " class='btn-clipboard'  data-original-title='Copy to clipboard'")
	  (button (concat "<div class=\"bd-clipboard\"> <button" btnprops " data-clipboard-target=\"#" label "\"> Copy </button> </div>")))
      (if (not lang) (format "<pre class=\"example\"%s>\n%s</pre>" label code)
	(format
	 "<div class=\"org-src-container\">\n%s%s\n</div>"
	 (if (not caption) ""
	   (format "<label class=\"org-src-name\">%s</label>"
		   (org-export-data caption info)))
	 (format "%s\n<pre class=\"src src-%s\" id=\"%s\">%s</pre>" button lang label code))))))

(defconst btsp-clipboardjs-script
"<!-- 2. Include library -->
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js\"></script>

    <!-- 3. Instantiate clipboard by passing a string selector -->
    <script>
    var clipboard = new ClipboardJS('.btn-clipboard');
    clipboard.on('success', function(e) {
        console.log(e);
    });
    clipboard.on('error', function(e) {
        console.log(e);
    });
    </script>"
"Clipoard JS script to be included"
)

(defun org-btsp-center-block (center-block contents info)
  "Transcode a CENTER-BLOCK element from Org to HTML.
CONTENTS holds the contents of the block.  INFO is a plist
holding contextual information."
  (format "<div class=\"text-center\">\n%s</div>" contents))

(defun org-html--tags (tags info)
  "Format TAGS into HTML.
INFO is a plist containing export options."
  (when tags
    (format "<span class=\"badge badge-pill badge-info\" style=\"font-size:60%%;\">%s</span>"
	    (mapconcat
	     (lambda (tag)
	       (format "<span class=\"%s\">%s</span>"
		       (concat (plist-get info :html-tag-class-prefix)
			       (org-html-fix-class-name tag))
		       tag))
	     tags "&#xa0;"))))

(defun org-btsp--toc-text (toc-entries)
  "Return innards of a table of contents, as a string.
TOC-ENTRIES is an alist where key is an entry title, as a string,
and value is its relative level, as an integer."
  (let* ((prev-level (1- (cdar toc-entries)))
	 (start-level prev-level))
    (concat
     (mapconcat
      (lambda (entry)
	(let ((headline (car entry))
	      (level (cdr entry)))
	  (concat
	   (let* ((cnt (- level prev-level))
		  (times (if (> cnt 0) (1- cnt) (- cnt))))
	     (setq prev-level level)
	     (concat
	      (org-html--make-string
	       times (cond ((> cnt 0) "\n<ul class=\"list-unstyled\" style=\"padding-inline-start: 20px;\">\n<li>")
			   ((< cnt 0) "</li>\n</ul>\n")))
	      (if (> cnt 0) "\n<ul class=\"list-unstyled\" style=\"padding-inline-start: 20px;\">\n<li>" "</li>\n<li>")))
	   headline)))
      toc-entries "")
     (org-html--make-string (- prev-level start-level) "</li>\n</ul>\n"))))

(defun org-btsp-toc (depth info &optional scope)
  "Build a table of contents.
DEPTH is an integer specifying the depth of the table.  INFO is
a plist used as a communication channel.  Optional argument SCOPE
is an element defining the scope of the table.  Return the table
of contents as a string, or nil if it is empty."
  (let ((toc-entries
	 (mapcar (lambda (headline)
		   (cons (org-html--format-toc-headline headline info)
			 (org-export-get-relative-level headline info)))
		 (org-export-collect-headlines info depth scope))))
    (when toc-entries
      (let ((toc (concat "<div id=\"text-table-of-contents\">"
			 (org-btsp--toc-text toc-entries)
			 "</div>\n")))
	(if scope toc
	  (let ((outer-tag (if (org-html--html5-fancy-p info)
			       "nav"
			     "div")))
	    (concat (format "<%s id=\"table-of-contents\">\n" outer-tag)
		    toc
		    (format "</%s>\n" outer-tag))))))))

(defun org-btsp--build-head (info)
  "Return information for the <head>..</head> of the HTML output.
INFO is a plist used as a communication channel."
  (org-element-normalize-string
   (concat
    (org-element-normalize-string (plist-get info :html-head))
    (when (plist-get info :html-head-include-default-style)
      (org-element-normalize-string org-btsp-style-default))
    (org-element-normalize-string (plist-get info :html-head-extra))
    (when (and (plist-get info :html-htmlized-css-url)
	       (eq org-html-htmlize-output-type 'css))
      (org-html-close-tag "link"
			  (format "rel=\"stylesheet\" href=\"%s\" type=\"text/css\""
				  (plist-get info :html-htmlized-css-url))
			  info))
    (when (plist-get info :html-head-include-scripts) org-btsp-scripts))))

(defun org-btsp-inner-template (contents info)
  "Return body of document string after HTML conversion.
CONTENTS is the transcoded contents string.  INFO is a plist
holding export options."
  (concat
   ;; Document contents.
   contents
   ;; Footnotes section.
   (org-html-footnote-section info)))

(defun btsp-html-template (contents info)
  (concat
   "<!DOCTYPE html>\n"
   (format "<html lang=\"%s\">\n" (plist-get info :language))
   "<head>\n"
   (org-html--build-meta-info info)
   (org-html--build-mathjax-config info)
   (org-btsp--build-head info)
   "</head>\n"
   "<body>\n"
   (org-element-normalize-string org-btsp-header)
   "<div class=\"container-fluid\">"
   ;; Document title.
   "<div class=\"page-header\">"
   (let ((title (plist-get info :title)))
     (format "<h1 class=\"bd-title text-center\" id=\"content\">%s</h1>\n" (org-export-data (or title "") info)))
   "</div>"
   "<div class=\"row flex-xl-nowrap\">"
   "<div class=\"col-12 col-md-3 col-xl-2 bd-sidebar\">"
   "<nav id=\"bd-docs-nav\" class=\"bd-links\">"
   (let ((depth (plist-get info :with-toc)))
     (when depth (org-btsp-toc depth info)))
   "</nav>"
   "</div>" ;; end of BD-SIDEBAR
   "<div class=\"d-none d-xl-block col-xl-2 bd-toc\">"
   
   "</div>" ;; end of BD-TOC
   "<main class=\"col-12 col-md-9 col-xl-8 py-md-3 pl-md-5 bd-content\">"
   contents
   "</main>"
   "</div>" ;; end of ROW
   "</div>" ;; end of CONTAINER FLUID
   "<button type=\"button\" onclick='topFunction()' id=\"gototop\">></span>"
   (org-element-normalize-string btsp-clipboardjs-script)
   "</body>\n"
   "</html>\n"))

(defun org-btsp-export-to-html
    (&optional async subtreep visible-only body-only ext-plist)
  "Export current buffer to a HTML file."
  (interactive)
  (let* ((extension (concat "." "html"))
         (file (org-export-output-file-name extension subtreep))
         (org-export-coding-system org-html-coding-system))
    (org-export-to-file 'btsp file
      async subtreep visible-only body-only ext-plist)))

;;;###autoload
(defun org-btsp-publish-to-html (plist filename pub-dir)
  "Publish an org file to HTML.
 
FILENAME is the filename of the Org file to be published.  PLIST
is the property list for the given project.  PUB-DIR is the
publishing directory.
 
Return output file name."
  (org-publish-org-to 'btsp filename
                      (concat "." (or (plist-get plist :html-extension)
                                      org-html-extension "html"))
                      plist pub-dir))

(provide 'btsp)

;; Local variables:
;; generated-autoload-file: "org-loaddefs.el"
;; End:

;;; btsp.el ends here
