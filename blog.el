#!/data/data/com.termux/files/usr/bin/emacs --script
;;; Source: https://github.com/mikedmcfarland/raindance.gdd/blob/0e959a0b6dce51d1c9b7dc9654959c8a973a22c9/exportOrg
;;; This script will build all the org files located under src

;; disable auto-save and auto-backup
(setq auto-save-default nil)
(setq make-backup-files nil)

(require 'org)



(load "~/.bin/lib/ox-pub/htmlize.el")
(load "~/.bin/lib/ox-pub/btsp.el")
(load "~/.bin/lib/ox-pub/ox-rss.el")

(defun org-blog-sitemap-format-entry (entry _style project)
  "Return string for each ENTRY in PROJECT."
  (format "@@html:<span class=\"archive-item\"><span class=\"archive-date\">@@ %s @@html:</span>@@ [[file:%s][%s]] @@html:</span>@@"
	  (format-time-string "%h %d, %Y"
			      (org-publish-find-date entry project))
	  entry
	  (org-publish-find-title entry project)))

(defun org-blog-sitemap-function (title list)
  "Return sitemap using TITLE and LIST returned by `org-blog-sitemap-format-entry'."
  (concat "#+TITLE: " title "\n\n"
	  "\n#+begin_archive\n"
	  (mapconcat (lambda (li)
		       (format "@@html:<li style=\"margin-left:25px\">@@ %s @@html:</li>@@" (car li)))
		     (seq-filter #'car (cdr list))
		     "\n")
	  "\n#+end_archive\n"))


(defvar blogsrc "~/ItsyBitsySpider/Blog/src"
  "Where to look for all the org files")

(defvar blogdest "~/ItsyBitsySpider/Blog/"
  "Where to build for all the org files")


;;OR Set your  publishing settings here
(princ (format "%s%s" "publishing org files in " blogsrc))
(setq org-publish-project-alist
      `(("myOrgProject"
	 :base-directory ,blogsrc
	 :base-extension "org"
	 :publishing-directory ,blogdest
	 :exclude ".*/index\.org\\|.*drafts/*"
	 :recursive t
         :author "Rizwan Ishak"
	 :publishing-function org-btsp-publish-to-html
	 
					;		 :html-postamble t
         :html-doctype "html5"
         :html-html5-fancy t
         :htmlized-source t
	 
	 :with-title t
	 :with-author t
	 :with-date nil
	 :with-toc t
	 :with-tasks t
	 :html-head-include-default-style nil
	 :html-head-extra "<link rel='stylesheet' href='/assets/css/stylesheet.css' />"
	 ;;	 :with-headline-numbers nil ;;TURNS OFF DISPLAY ONLY
	 
	 :preserve-breaks t
	 :headline-levels 2
	 :section-numbers nil
	 
	 :auto-sitemap t
	 :sitemap-filename "index.org"
	 :sitemap-title "Blog Posts"
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 :sitemap-format-entry org-blog-sitemap-format-entry
	 :sitemap-function org-blog-sitemap-function
	 )
	("assets"
	 :base-directory "~/ItsyBitsySpider/Blog/src/assets/"
	 :base-extension ".*"
	 :publishing-directory "~/ItsyBitsySpider/Blog/assets/"
	 :publishing-function org-publish-attachment
	 :recursive t)
	("indexes"
	 :base-directory "~/ItsyBitsySpider/Blog/src"
	 :base-extension "org"
	 :exclude ".*"
	 :include ("posts/tech/index.org" "posts/misc/index.org" "posts/scripture/index.org" "posts/readings/index.org" "posts/acad/index.org")
	 :publishing-directory "~/ItsyBitsySpider/Blog/"
	 :publishing-function org-btsp-publish-to-html
	 :html-head-include-default-style nil
	 :html-head-extra "<link rel='stylesheet' href='/assets/css/stylesheet.css' />"
	 :section-numbers nil
	 :recursive t)
	("rss"
	 :base-directory "~/ItsyBitsySpider/Blog/src"
	 :base-extension "org"
	 :html-link-home ""
	 :html-link-use-abs-url t
	 :rss-extension "xml"
	 :publishing-directory "~/ItsyBitsySpider/Blog"
	 :publishing-function (org-rss-publish-to-rss)
	 :exclude ".*"
	 :include ("index.org")
	 :section-numbers nil
	 :table-of-contents nil)

	("blogin" :components ( "myOrgProject" "assets" "indexes" "rss"))
	))

(if (equal (elt argv 0) "force")
    (org-publish-project "blogin" t)
  (org-publish-project "blogin"))

(princ "\norg publish complete")
