#!/usr/bin/emacs --script
;;; Source: https://github.com/mikedmcfarland/raindance.gdd/blob/0e959a0b6dce51d1c9b7dc9654959c8a973a22c9/exportOrg
;;; This script will build all the org files located under src

;; disable auto-save and auto-backup
(setq auto-save-default nil)
(setq make-backup-files nil)

(require 'org)


 (load "~/.bin/lib/ox-pub/htmlize.el")
 (load "~/.bin/lib/ox-pub/ox-twbs.el")

(defvar src (elt argv 0)
  "Where to look for all the org files")

(defvar dest (elt argv 1)
  "Where to build for all the org files")

(princ (format "%s%s" "publishing org files in " src))
(setq org-publish-project-alist
      `(("myOrgProject"
	 :base-directory ,src
	 :base-extension "org"
	 :publishing-directory ,dest

	 :recursive t
         :author "Rizwan Ishak"
	 :publishing-function org-twbs-publish-to-html
	 
	 :html-extension "html"
	 :html-postamble t
         :html-doctype "html5"
         :html-html5-fancy t
         :htmlized-source t
	 
	 :with-title t
	 :with-author t
	 :with-date nil
	 :with-toc t
	 :with-tasks t
	 :with-headline-numbers nil ;;TURNS OFF DISPLAY ONLY
	 
	 :preserve-breaks t
	 :headline-levels 2
	 :section-numbers t
					;		 :toc t
	 
	 :auto-sitemap t
	 :sitemap-filename "archive.org"
	 :sitemap-title "Posts"
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )))

(if (equal (elt argv 2) "force")
    (org-publish-project "myOrgProject" t)
  (org-publish-project "myOrgProject"))

(princ "\norg publish complete")
